<?php

namespace app\models;

use app\services\MarksManager;
use Yii;

/**
 * This is the model class for table "doer_project".
 *
 * @property integer $id
 * @property integer $doer_id
 * @property integer $project_id
 *
 * @property Doer $doer
 * @property Project $project
 */
class DoerProject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'doer_project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['doer_id', 'project_id'], 'integer'],
            [['doer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Doer::className(), 'targetAttribute' => ['doer_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'doer_id' => 'Doer ID',
            'project_id' => 'Project ID',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $doer = $this->doer;
        $professions = $doer->getProfessionsId();
        $doer->projects_count++;
        $doer->save();

        $criterions = Criterions::find()->all();
        foreach($professions as $profession)
        {
            foreach($criterions as $criterion)
            {
                $mark = new Mark;
                $mark->doer_id = $doer->id;
                $mark->project_id = $this->project_id;
                $mark->profession_id = $profession;
                $mark->criterion_id = $criterion->id;
                $mark->mark = 0;
                $mark->save();
            }
        }

    }

    public function afterDelete()
    {
        parent::afterDelete();

        $doer = $this->doer;
        $doer->projects_count--;
        $marks = Mark::find()->where(['project_id' => $this->project_id, 'doer_id' => $this->doer_id])->all();
        foreach($marks as $mark)
        {
            $mark->delete();
        }

        $doer->save();

        MarksManager::recountRating($doer->id);

    }

    /**
     * Создает запись в таблице, которая связывает исполителя с заказом
     * @param $doer_id
     * @param $project_id
     */
    public function saveRelation($doer_id, $project_id)
    {
        $this->doer_id = $doer_id;
        $this->project_id = $project_id;
        $this->save();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoer()
    {
        return $this->hasOne(Doer::className(), ['id' => 'doer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

}
