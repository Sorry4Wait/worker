<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "black_list".
 *
 * @property int $id
 * @property int $from_id
 * @property int $block_id
 */
class BlackList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'black_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['from_id', 'block_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ИД',
            'from_id' => 'Кто добавил в черный список',
            'block_id' => 'Кого добавил',
        ];
    }

}
