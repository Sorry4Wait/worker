<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Doer;

/**
 * DoerSearch represents the model behind the search form about `app\models\Doer`.
 */
class DoerSearch extends Doer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'projects_count'], 'integer'],
            [['snp', 'phone', 'email', /*'skype'*/'middle_mark', 'education', 'other_professions', 'specific', 'comment', 'main_professions'], 'safe'],
            [['middle_mark'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Doer::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
            'middle_mark' => $this->middle_mark,
            'projects_count' => $this->projects_count,
        ]);

        $query->andFilterWhere(['like', 'snp', $this->snp])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
//            ->andFilterWhere(['like', 'skype', $this->skype])
            ->andFilterWhere(['like', 'middle_mark', $this->middle_mark])
            ->andFilterWhere(['like', 'education', $this->education])
            ->andFilterWhere(['like', 'other_professions', $this->other_professions])
            ->andFilterWhere(['like', 'main_professions', $this->main_professions])
            ->andFilterWhere(['like', 'specific', $this->specific])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
