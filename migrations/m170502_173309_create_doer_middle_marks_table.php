<?php

use yii\db\Migration;

/**
 * Handles the creation of table `doer_middle_marks`.
 */
class m170502_173309_create_doer_middle_marks_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('doer_middle_marks', [
            'id' => $this->primaryKey(),
            'doer_id' => $this->integer()->notNull(),
            'profession_id' => $this->integer()->notNull(),
            'middle_mark' => $this->float(),
        ]);

        $this->createIndex(
            'idx-doer_middle_marks-doer_id',
            'doer_middle_marks',
            'doer_id'
        );

        $this->addForeignKey(
            'fk-doer_middle_marks-doer_id',
            'doer_middle_marks',
            'doer_id',
            'doer',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-doer_middle_marks-profession_id',
            'doer_middle_marks',
            'profession_id'
        );

        $this->addForeignKey(
            'fk-doer_middle_marks-profession_id',
            'doer_middle_marks',
            'profession_id',
            'profession',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('doer_middle_marks');
    }
}
