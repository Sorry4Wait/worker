<?php

use yii\db\Migration;

/**
 * Handles dropping middle_mark from table `project`.
 */
class m170501_100934_drop_middle_mark_column_from_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('project', 'middle_mark');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('project', 'middle_mark', $this->float());
    }
}
