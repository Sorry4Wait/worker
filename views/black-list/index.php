<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BlackListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Черный список';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-inverse black-list-index">
    <div class="panel-heading">
        <!--        <div class="panel-heading-btn">-->
        <!--        </div>-->
        <h4 class="panel-title">Черный список</h4>
    </div>
    <div class="panel-body">
        <div id="ajaxCrudDatatable">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'from_id',
                'value' => function ($model){
                    $user = \app\models\User::findOne(['id' => $model->from_id]);
                    return $user->login;
                },

            ],

            [
                'attribute' => 'block_id',
                'value' => function ($model){
                   $user = \app\models\Doer::findOne(['id' => $model->block_id]);
                   return $user->snp;
                },

            ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
    </div>
</div>
