<?php
use app\models\Cities;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'snp',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'phone',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'email',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'middle_mark',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'main_professions',
        'value' => function($model){

            $id = (is_array($model)) ? $model['id'] : $model->id;
            if(is_array($model)) {
                $ot_pref = json_decode($model['main_professions']);
            }else{
                $ot_pref = $model['main_professions'];
            }

            $mark = '';
            if(!empty($ot_pref)){
                foreach($ot_pref as $pr){
                    $profName =  \app\models\Profession::find()->select('*')->where(['id' => $pr])->one();

                    $doerMiddleMarks = \app\models\DoerMiddleMarks::find('*')->where(['profession_id' => $profName->id])->andWhere(['doer_id' => $id])->one();
                    if($doerMiddleMarks == null){
                        $mark .= ' '.$profName->name.'(0)';
                    }else{
                        $mark .= ' '.$profName->name.'('.$doerMiddleMarks->middle_mark.'),';
                    }


                }
            }

            return $mark;
        },
        'filterInputOptions' => [
            'class' => 'form-control',
            'prompt' => 'Все',
        ],
        'filter'=> $model->getProfessionList(),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'other_professions',
        'value' => function($model){
            $id = (is_array($model)) ? $model['id'] : $model->id;
            if(is_array($model)) {
                $ot_pref = json_decode($model['other_professions']);
            }else{
                $ot_pref = $model['other_professions'];
            }

            $mark = '';
            if(!empty($ot_pref)){
                foreach($ot_pref as $pr){
                    $profName =  \app\models\Profession::find()->select('*')->where(['id' => $pr])->one();
                    $doerMiddleMarks = \app\models\DoerMiddleMarks::find('middle_mark')->where(['profession_id' => $profName->id])->andWhere(['doer_id' => $id])->one();
                    if($doerMiddleMarks == null){
                        $mark .= ' '.$profName->name.'(0)';
                    }else{
                        $mark .= ' '.$profName->name.'('.$doerMiddleMarks->middle_mark.'),';
                    }
                }
            }
            return $mark;
        },
        'filterInputOptions' => [
            'class' => 'form-control',
            'prompt' => 'Все',
        ],
        'filter'=> $model->getProfessionList(),

    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'projects_count',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'city_id',
        'value' => function($model){
            $id = (is_array($model)) ? $model['city_id'] : $model->city_id;

            $city = Cities::find()->where(['id' => $id])->one();

            return $city['name'];
        },
        'filterInputOptions' => [
            'class' => 'form-control',
            'prompt' => 'Все',
        ],
        'filter'=> $model->getCitiesList(),
        'header' => 'Город',

    ],
    [

        'attribute'=>'black',
        'content' => function($model){


            if(\app\models\BlackList::find()->where(['block_id' => $model['id']])->exists()){
                return 'Уже В черном списке!';
            }
            return Html::a('<span class="fa fa-ban"> В черный список</span>', ['black-list/pluse','id' => $model['id']]);
        },
        'format' => 'raw',

    ],


];