<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Doer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="doer-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'snp')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'skype')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'city_id')->dropDownList($model->getCitiesList()) ?>
        </div>
    </div>

    <?= $form->field($model, 'education')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'main_professions')->widget(Select2::classname(), [
        'data' => $model->getProfessionList(),
        'options' => ['placeholder' => 'Выберите ...'],
        'pluginEvents' => [
            "change" => "function() 
						{
							
						}",
        ],
        'pluginOptions' => [
            'allowClear' => true,
            'multiple' => true,
        ],
    ]);?>

    <?= $form->field($model, 'other_professions')->widget(Select2::classname(), [
        'data' => $model->getProfessionList(),
        'options' => ['placeholder' => 'Выберите ...'],
        'pluginEvents' => [
            "change" => "function() 
						{
							
						}",
        ],
        'pluginOptions' => [
            'allowClear' => true,
            'multiple' => true,
        ],
    ]);?>

    <?= $form->field($model, 'specific')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
