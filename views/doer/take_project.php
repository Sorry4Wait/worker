<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 02.05.2017
 * Time: 6:34
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>

<div class="doer-take_order">
    <div class="doer-take_order-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'project_id')->dropDownList($doer->freeProjects) ?>

        <?php if (!Yii::$app->request->isAjax){ ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>
</div>
