<?php
use yii\helpers\Url;


return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
/*    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_creator_id',
    ],*/
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date_created',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'date_updated',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) use ($doer_id) {
            if($action == 'delete')
            {
                return Url::to(['doer/remove-project','doer_id' => $doer_id, 'project_id' => $model->id]);
            }
            if($action == 'view')
            {
                return Url::to(['project/view','id' => $model->id, 'doer_id' => $doer_id]);
            }
            return Url::to(['project/'.$action,'id' => $key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Сохранить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'url' => Url::toRoute(['doer/remove-project', 'doer_id' => $doer_id, 'project_id' => isset($id) ? $id : null]),
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Вы уверены?',
            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'],
    ],

];