<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\DetailView;
use rmrevin\yii\module\Comments;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Проекты";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="panel panel-inverse project-index">
    <div class="panel-heading">
        <!--        <div class="panel-heading-btn">-->
        <!--        </div>-->
        <h4 class="panel-title"><?=$model->snp?></h4>
    </div>
    <div class="panel-body">
        <div id="ajaxCrudDatatable">

            <?= Comments\widgets\CommentListWidget::widget([
                'entity' => (string)"task-{$model->id}",// type and id
                'showDeleted' => false,
                'showCreateForm' => $user->role == 'admin' ? true : false,
            ]); ?>
            <hr />
            <?=GridView::widget([
                'id'=>'crud-datatable',
                'dataProvider' => $ordersDataProvider,
                'filterModel' => $ordersSearchModel,
                'pjax'=>true,
                'pjaxSettings'=>[
                    'neverTimeout'=>true,
                    'beforeGrid'=> DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'snp',
                            'middle_mark',
                            'phone',
                            'email:email',
                            'projects_count',
                        ],
                    ]).$this->render('_marks', ['doer_id' => $doer_id]),
                ],
                'columns' => require(__DIR__.'/_orders_columns.php'),
                'toolbar'=> [
                    ['content'=>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['doer/take-project', 'doer_id' => $doer_id],
                            ['role'=>'modal-remote','title'=> 'Привязать проект','class'=>'btn btn-default']).
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['', 'id' => $doer_id],
                            ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Обновить']).
                        '{toggleData}'.
                        '{export}'
                    ],
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'headingOptions' => ['style' => 'display: none;'],
                    'after'=>BulkButtonWidget::widget([
                            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
                                ["project/bulk-delete"],
                                [
                                    "class"=>"btn btn-danger btn-xs",
                                    'role'=>'modal-remote-bulk',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Вы уверены?',
                                    'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'
                                ]),
                        ]).
                        '<div class="clearfix"></div>',
                ]
            ])?>
        </div>

    </div>
</div>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>+
<?php Modal::end(); ?>


